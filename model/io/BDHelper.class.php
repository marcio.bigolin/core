<?php

/**
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0.0
 */
class BDHelper
{

    /**
     *
     * @var PDO 
     */
    protected $database;
    private $debug;
    private $lastQuery = '';
    private $inTrasaction = false;
    private static $strConexao = '';
    private static $conexao = null;
    private $error = array();

    /**
     * 
     * @param type $dbName
     * @param type $databaseType
     * @param type $dbServer
     * @param type $user
     * @param type $password
     */
    public function __construct($dbName, $databaseType, $dbServer, $user, $password)
    {
        $str = $databaseType . ':host=' . $dbServer . ';dbname=' . $dbName;
        $this->database = $this->verificaConexao($str, $user, $password);
        $this->debugOn(false);
        $this->database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private function verificaConexao($str, $user, $password)
    {
        if (self::$conexao != null && self::$strConexao == $str . $user . $password) {
            return self::$conexao;
        } else {
            self::$strConexao = $str . $user . $password;
            self::$conexao = new PDO($str, $user, $password);
            return self::$conexao;
        }
    }

    public function debugOn($status = true)
    {
        $this->debug = $status;
    }

    public function begin()
    {
        $this->inTrasaction = true;
        return $this->database->beginTransaction();
    }

    public function rollback()
    {
        $this->inTrasaction = false;
        return $this->database->rollBack();
    }

    public function commit()
    {
        $this->inTrasaction = false;
        return $this->database->commit();
    }

    public function inTransaction()
    {
        return $this->inTrasaction;
    }

    public function saveFile($table, $colunm, $file, $extras = array())
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    /**
     * Método que prepara o LargeObject ou o array de bytes para salvar em coluna
     * 
     * @param type $file
     * @param type [$type] Tipo do salvamento em coluna byte ou largeObject - Padrão lo
     */
    public function saveFileInColunm($file, $type = 'lo')
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    /**
     * Método que le um arquivo do banco de dados
     * 
     * @param string $table - nome da tabela
     * @param string $colunm
     * @param int $id - seletor
     * @param type $extras - Opções extras que podem ser usados 
     * @throws ProgramacaoException
     */
    public function readFile($table, $colunm, $file, $extras = array())
    {
        throw new ProgramacaoException('Código não implementado para o SGBD em uso');
    }

    private function verificaArquivo($arquivo)
    {
        if (get_class($arquivo) == 'ArquivoUpload') {
            $data = $this->saveFileInColunm($arquivo->getArquivo(), $arquivo->getBDType());
            $arquivo->setDataBD($data);
        }
    }

    public function queryAsArray($sql)
    {
        $return = array();
        $data = array();

        foreach ($this->database->query($sql) as $return) {
            array_push($data, $return);
        }

        return $data;
    }

    /**
     * 
     * @param type $sql
     * @return PDOStatement
     * @throws SQLException
     */
    public function query($sql)
    {
        $this->lastQuery = $sql;
        if ($this->debug) {
            DebugUtil::showCode($sql);
        }
        try {
            return $this->database->query($sql);
        } catch (PDOException $exception) {
            throw new SQLException($exception, $sql);
        }
    }

    /**
     * Método que monta uma query SQL 
     * 
     * @param type $table
     * @param type [$fields]
     * @param misc [$condition] Se String aplica a condição caso, for um array usa Prepared
     * @param type [$order]
     * @param type [$limit]
     * @return SQLPointer
     */
    public function queryTable($table, $fields = '*', $condition = null, $order = null, $limit = null)
    {
        $sql = 'SELECT ' . $fields . ' FROM ' . $table;

        if (isset($condition) && $condition) {
            if (is_array($condition)) {
                //ver de montar o prepared
            }
            $sql .= ' WHERE ' . $condition;
        }

        if (!empty($order)) {
            $sql .= ' ORDER BY ' . $order;
        }

        if (!empty($limit)) {
            $sql .= ' LIMIT ' . $limit;
        }
        return $this->query($sql);
    }

    private function geraStringDeInsercao(&$data, $key, $value)
    {
        $valor = '?';
        if (is_object($value)) {
            unset($data[$key]);
            $this->verificaArquivo($value);
            $valor = $value->codigoInsercao();
        }
        return $valor;
    }

    /**
     * Realiza um insert no banco de dados.
     * 
     * @param String $table
     * @param misc $data
     * @return boolean
     */
    public function insert($table, $data)
    {
        $sql = 'INSERT INTO ' . $table . ' ';

        //Verifica se é array para percorrer com foreach
        if (is_array($data)) {
            $sql .= '(';
            $values = '';
            //Acrescenta o nome dos campos e valida objetos
            foreach ($data as $name => $value) {
                $sql .= $name . ',';
                $values .= $this->geraStringDeInsercao($data, $name, $value) . ',';                        
            }

            //Retira a última vírgula, para não ser necessário fazer um contador
            $sql = trim($sql, ',');
            $sql .= ') ';
            $sql .= 'VALUES (' . trim($values, ',') . ')';
            return $this->execute($sql, $data);
        } else {
            $sql .= $data;
        }

        return $this->exec($sql);
    }

    public function update($table, $data, $condition)
    {
        $sql = 'UPDATE ' . $table . ' SET ';
        $isArray = false;
        //Verifica se é objeto ou array para percorrer com foreach
        if (is_array($data)) {
            //Percorre os campos e os valores
            foreach ($data as $name => $value) {
                $valor = $this->geraStringDeInsercao($data, $name, $value);
                $sql .= $name . '= ' . $valor . ' ,';
            }
            $sql = trim($sql, ','); //Retira a vírgula excedente
            $isArray = true;
        } else {
            $sql .= $data;
        }
        if (isset($condition)) {
            $sql .= ' WHERE ' . $condition;
        }
        if ($isArray) {
            return $this->execute($sql, $data);
        }

        return $this->exec($sql);
    }

    public function getLastQuery()
    {
        return $this->lastQuery;
    }

    public function delete($table, $condition)
    {
        if (is_array($condition)) {
            $condicao = '';
            foreach ($variable as $key => $value) {
                $condicao .= $key . ' = ' . $value;
            }
            $condition = $condicao;
        }

        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $condition;
        return $this->exec($sql);
    }

    /**
     * 
     * @param String $sql
     * @param Array $data
     * @return bool <b>TRUE</b> no caso de sucesso ou <b>FALSE</b> caso ocora alguma falha.
     * @throws SQLException
     */
    private function execute($sql, $data)
    {
        try {
            if ($this->debug) {
                DebugUtil::showCode($sql);
                DebugUtil::show($data);
            }
            $this->lastQuery = $sql;
            $stmt = $this->database->prepare($sql);
            return $stmt->execute(array_values($data));
        } catch (PDOException $exception) {
            if ($this->debug) {
                throw new SQLException($exception, $sql, array_values($data));
            }
            $this->error[] = [$exception, $sql, array_values($data)];
            return false;
        }
    }

    /**
     * Função que executa uma string de SQL no banco de dados
     * 
     * @param type $sql
     * @return int|bool numeros de linhas e FALSE em caso de falha
     * @throws SQLException
     */
    public function exec($sql)
    {
        try {
            if ($this->debug) {
                DebugUtil::showCode($sql);
            }
            $this->lastQuery = $sql;
            return $this->database->exec($sql);
        } catch (PDOException $exception) {
            if ($this->debug) {
                throw new SQLException($exception, $sql);
            }
            $this->error[] = [$exception, $sql];
            return false;
        }
    }

    /**
     * 
     * @param String $name sequence_name ou tabela
     * @return type
     */
    public function lastInsertId($name)
    {
        return $this->database->lastInsertId($name);
    }

    public function getLogErrors()
    {
        return $this->error;
    }

    /**
     * 
     */
    public function __destruct()
    {
        if ($this->inTrasaction) {
            $this->commit(); //FIXME ver se não é melhor lançar uma exceção
        }
        $this->database = null;
    }

}
