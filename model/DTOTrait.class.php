<?php

namespace core\model;

/**
 * A DTO Trait é uma trait que tem por função dar manipular de forma mais adequada
 * e de forma mais transparente todos atributos de um objeto DTO.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.c
 */
trait DTOTrait
{
   
    /**
     * Adiciona em tempo de execução um campo para ser ignorado. 
     * 
     * @param String $field nome do campo a ser ignorado
     */
    public function ignoreField($fields)
    {
        if (!isset($this->ignoreFields)) {
            $this->ignoreFields = array();
        }
        $args = func_get_args();
        if (is_array($fields)) {
            $this->ignoreFields = array_merge($this->ignoreFields, $fields);
        } else if(sizeof ($args)> 1){
            foreach($args as $field){
                $this->ignoreFields[] = $field;
            }
        } else{
            $this->ignoreFields[] = $fields;
        }
    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para inserir no banco
     * 
     * @return Array - Array de dados para inserir 
     */
    public function getArrayDados()
    {
        $campos = array();
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor)) {
                $campos[\StringUtil::underscoreNumber($chave)] = $valor;
            }
        }
        return $campos;
    }

    /**
     * Retorna um array com o formato 
     *     "campo_tabela" => "valor" 
     * 
     * para atualizar tupla em uma tabela no banco
     * 
     * @return Array - Array de dados para atualizar 
     */
    public function getArrayAtualizar()
    {
        $campos = array();
        foreach ($this as $chave => $valor) {
            if ($this->verificaCampo($chave, $valor) && $valor !== null) {
                $campos[\StringUtil::toUnderscore($chave)] = $valor;
            }
        }
        return $campos;
    }

    /**
     * Popula o objeto recebendo um array no formato
     * "nomeCampo" => "valor" 
     *
     * @param Array $array - Array de 
     * @return Integer - número de erros encontrados
     */
    public function setArrayDados($array)
    {
        $erros = 0;
        foreach ($array as $campo => $valor) {
            $metodo = 'set' . ucfirst($campo);
            if (method_exists($this, $metodo) && !$this->{$metodo}($this->trataValor($valor))) {
                $erros ++;
                $this->isValid = false;
            }
        }
        return $erros;
    }

    public function load($tabela, $id)
    {
        $daoName = \StringUtil::toCamelCase($tabela, 1) . 'DAO';
        $dao = new $daoName();
        return $dao->getById($id);
    }

    public function loadList($tabela, $condicao)
    {
        $daoName = \StringUtil::toCamelCase($tabela, 1) . 'DAO';
        $dao = new $daoName();
        return $dao->getLista($condicao);
    }

    /**
     * Recebe valores através de variável valor e caso necessário
     * faz o parser dos objetos como o caso de objetos geográficos ou arquivos.
     * 
     * @param misc $valor
     * @return misc pode ser o próprio valor ou um objeto.
     */
    private function trataValor($valor)
    {
        if (is_string($valor) && strpos($valor, 'POINT(') !== false) {
            return new \Point($valor);
        }
        return $valor;
    }

    /**
     * Verifica se o parametro do objeto vai ser usado para o 
     * mapeamento objeto relacional. 
     *      * 
     * @param type $chave
     * @param type $valor
     * @return boolean
     */
    private function verificaCampo($chave, $valor)
    {
        $ignore = isset($this->ignoreFields) ? !(in_array($chave, $this->ignoreFields)) : true;
        return (( strcasecmp($chave, 'id' . __CLASS__) != 0) && ($chave != 'isValid') && ($chave != 'table') && ($chave != 'ignoreFields') && $ignore && ($chave[0] != '_') && !is_array($valor));
    }
    
    

}
