<?php

/**
 * A classe AbstractDAO possui os métodos bases dos objetos DAO. 
 * 
 * Também determina que métodos devem ser gerados de forma especificas em cada um 
 * dos objetos filhos.
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.0
 * @package core.model
 */
abstract class AbstractDAO extends AbstractModel
{

    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Método que retorna um ponteiro para um arquivo ou null caso alguma condição
     * não for satisfeita.
     * 
     * @param $colunaArquivo
     * @param $id
     */
    public function getFile($colunaArquivo, $id){
       $this->DB()->begin();
       return $this->DB()->readFile($this->getTable(), $colunaArquivo, $id); 
    }

    /**
     * Método que insere se for um objeto novo ou atualiza se for um existente
     * 
     * @param DTOInterface $object
     * @return bool
     */
    public function save(DTOInterface $object)
    {
        $id = $object->getID();
        if (empty($id)) {
            return $this->create($object);
        } else {
            return $this->update($object);
        }
    }

    /**
     * Método que insere um objeto em sua respectiva tabela no banco de dados
     *
     * @param DTOInterface Objeto data transfer
     */
    public function create(DTOInterface $object)
    {
        return $this->db->insert($object->getTable(), $object->getArrayDados());
    }
    
   
    /**
     * Retorna um array com todos os objetos.
     * 
     * @return Array DTOInterface 
     */
    public function getAll($condicao = false)
    {
        return $this->getLista($condicao);
    }
    
    /**
     * Retorna um array com todos os objetos de acordo com a condição.
     * 
     * @return Array DTOInterface 
     */
    public function getAllByCondition($condicao)
    {
        return $this->getLista($condicao);
    }

    public function update(DTOInterface $object)
    {
        return $this->db->update($object->getTable(), $object->getArrayAtualizar(), $object->getCondition());
    }

    /**
     * Alias para getById pode ser sobscrito para se utilizar o comparable.
     * 
     * @param DTOInterface $object
     * @return type
     */
    public function getOne(DTOInterface $object)
    {
        return $this->getByID($object->getID());
    }

    /**
     * 
     * @param type $condicao
     * @return DTOInterface
     */
    public function getOneByCondition($condicao)
    {
        try {
            $lista = $this->getLista($condicao);
            if ($lista) {
                #TODO issue #17 - Verificar a performance de logar se caso retornou mais de um resultado.
                return $lista[0];
            }
            return false;
        } catch (ErrorException $e) {
            ds('LastQuery: ' . $this->DB()->getLastQuery());
            echo $e->getTraceAsString();
        }
    }

    /**
     * 
     * Depreciado pois foi implementado no gerador de CRUD o método especifico que
     * retorna um array de objetos com a chava como ID.
     * 
     * @param type $coluna
     * @param DTOInterface $object
     * @return type
     * @deprecated since version 2.0
     */
    public function getMapa($coluna, DTOInterface $object)
    {
        return $this->getMapaSimplesDados($this->queryTable($object->getTable()), $object->getID(), $coluna);
    }

    /**
     * Remove o objeto do banco de dados
     * 
     * @param DTOInterface $object
     * @return boolean
     */
    public function delete(DTOInterface $object)
    {
        return $this->db->delete($object->getTable(), $object->getCondition());
    }

    /**
     * 
     * @param Misc $id
     */
    public function getByID($id)
    {
        $consulta = $this->queryTable($this->getTable(), $this->getColunmsString(), $this->getColunmID() . '=' . $id);
        if ($consulta) {
            $objeto = $this->setDados($consulta->fetch());
            return $objeto;
        } else {
            throw new EntradaDeDadosException('Id inválido ' . $id);
        }
    }

    /**
     * Método que insere um objeto do tipo DTOInterface
     * na tabela do banco de dados
     *
     * @param DTOInterface Objeto data transfer
     */
    public function inserirEmTransacao(DTOInterface $obj)
    {
        $this->DB()->begin();
        if ($this->save($obj)) {
            $sequencia = $this->getTable() . '_' . $this->getColunmID() . '_seq';
            $id = $this->DB()->lastInsertId($sequencia);
            $this->DB()->commit();
            return $id;
        } else {
            $this->DB()->rollback();
            return false;
        }
    }

    /**
     * 
     * @deprecated since version 3.0 Usar getList mantido para retrocompatibilidade.
     * @param Misc $condicao
     * @return Array
     */
    public function getLista($condicao){
        return $this->getList($condicao);
    }
    
    /**
     * Método que permite retornar listas de DAOs especificos 
     * 
     * Método mais completo que permite ordenação e limit. 
     * 
     *  
     * @param Misc $condition
     * @param String [$order] Se informado aplica a ordenação do argumento
     * @param String [$limit] Se informado aplica o limite do argumento
     * @return Array
     */
    public function getList($condition,  $order = false, $limit = false )
    {
        $dados = array();
        $result = $this->queryTable($this->getTable(), $this->getColunmsString(), $condition, $order, $limit);
        if ($result) {
            foreach ($result as $linhaBanco) {
                $objeto = $this->setDados($linhaBanco);
                $dados[] = $objeto;
            }
        }
        return $dados;
    }

    /**
     * Retorna um array com a tabela dos dados para o componente de tabela
     *
     * @return Array tabela de dados
     */
    public function getTabela(TabelaConsulta $tabela)
    {
        $dados = array();
        $nLinhasCon = $this->queryTable($this->getTable(), 'count(' . $this->getColunmID() . ') as num');
        $nLinhas = $nLinhasCon->fetch();
        $result = $this->queryTable($this->getTable() . ' ' . $tabela->getcondicao(),
                $this->getColunmsString()
        );
        $resultado = array(
            'page' => $tabela->getPagina(),
            'total' => $tabela->calculaPaginacao($nLinhas['num']),
            'records' => $nLinhas['num']
        );
        foreach ($result as $linhaBanco) {
            $row = $row2 = array();
            $objeto = $this->setDados($linhaBanco);
            $id = $objeto->getID();
            $row['id'] = $id;
            $row['cell'] = $objeto->getArrayJson();            
            array_unshift($row['cell'], $id);
            $dados[] = $row;
        }
        $resultado['rows'] = $dados;
        return $resultado;
    }

    protected abstract function setDados($array); //Somente quando tiver todo o sistema testado

    /**
     * Método que retorna uma string montando o nome das colunas que devem ser buscadas
     * na tabela especifica
     * 
     */
    protected function getColunms()
    {
        return $this->colunms;
    }

    protected function getColunmsString()
    {
        $string = $this->getColunmID() . ' as principal, ';
        $string .= rtrim(implode(', ', $this->getColunms()), ',');
        return $string;
    }

    /**
     * Nome da tabela 
     * 
     * @return String 
     */
    protected function getTable()
    {
        return $this->table;
    }

    protected function getColunmID()
    {
        return $this->colunmID;
    }

    /**
     * Retorna todos os resultados mas com o ID como chave da lista
     * 
     */
    public function getAllWithID()
    {
        $dados = array();
        $result = $this->queryTable($this->getTable(), $this->getColunmsString());
        if ($result) {
            foreach ($result as $linhaBanco) {
                $objeto = $this->setDados($linhaBanco);
                $dados[$linhaBanco['principal']] = $objeto;
            }
        }
        return $dados;
    }

    /**
     * 
     */
    private function isValid()
    {
        
    }

}
