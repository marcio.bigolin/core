<?php

/**
 * Description of AbstractController
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package core.controller 
 */
abstract class AbstractController {
    use \core\controller\StorageTrait;
    use \core\controller\ToolsTrait;
    /**
     *
     * @var AbstractView  
     */
    protected $view;
    
    /**
     * Método padrão chamado para o controlador caso a ação não seja especificada
     */
    public abstract function index();
    
    public abstract function paginaNaoEncontrada();

    /**
     * 
     * @param string $url
     */
    public function redirect($url){
        if(!empty($url)){
            header('Location: '. $url);
        }
    }    
    
    /**
     * Captura todos os argumentos extras passados para o método via URL
     * 
     * @param int $pos posição do argumento extra começa com 0
     * @param CONST $filter Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return string
     */
    protected function getArg($pos, $tipo = FILTER_SANITIZE_STRING){
        if(isset($GLOBALS['ARGS'][$pos])){
            return filter_var($GLOBALS['ARGS'][$pos], $tipo);
        }
        return '';
    }
    
    /**
     * Garante o retorno de um Int.
     * 
     * @param misc $value
     * @return int
     */
    protected function trataInt($value){
        return filter_var($value, FILTER_SANITIZE_NUMBER_INT); 
    }
    
    /**
     * Trata o campo para não aceitar uma string maliciosa.
     * 
     * @param type $value
     * @return type
     */
    protected function trataString($value){
        return filter_var($value, FILTER_SANITIZE_STRING); 
    }
    
    /**
     * Verifica se a váriavel POST existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário mudar a variável opcional FILTER 
     * 
     * @param String $indice
     * @param CONST [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return misc Valor da variavel null no caso de não existir
     */
    protected function trataPost($indice, $filter = FILTER_SANITIZE_STRING){
        return filter_input(INPUT_POST, $indice, $filter);
    }
    
     /**
     * Verifica se a váriavel GET existe e a trata de forma adequada para uma string.
     * 
     * Atenção: Caso queira usar valores HTML é necessário verificar a variável de outra maneira 
     * 
     * @param String $indice
     * @param CONST [$filter] Filtro de Sanitize que pode ser utilizado por padrão Trata uma string
     * @return misc Valor da variavel null no caso de não existir
     */
    protected function trataGet($indice, $filter = FILTER_SANITIZE_STRING){
        return filter_input(INPUT_GET, $indice, $filter);
    }

    /**
     * 
     * @param String $message
     */    
    protected function error400($message){
        $this->view->setRenderizado(); 
        http_response_code(400);
        echo $message;
        exit();
    }

    public function __destruct() {
        if(isset($this->view) && !$this->view->isRender()){
            $this->view->render();
        }
    }
    
    public function logoEny(){
        $this->view->setRenderizado();

        //header('Content-type: image/svg+xml');
        echo file_get_contents(CORE . 'doc/Logotipia/eny.svg');
    }
    
    public function aboutEny(){
        $string = file_get_contents(CORE . 'enyalius');
        $inicio = strpos($string, '# Versão');
        echo 'Versão: ' . substr($string, $inicio+10,3);
        echo '<br> Mais informações: <a href="https://gitlab.com/enyalius">https://gitlab.com/enyalius</a>';
        $this->view->setRenderizado();
    }
}
