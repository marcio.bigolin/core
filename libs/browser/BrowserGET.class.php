<?php
namespace core\libs\browser;

/**
 * Description of BrowserGET
 *
 * @author marcio
 */
class BrowserGET extends Browser
{
    private $dataRequest = array();
   public function __construct($cookieFile = false)
   {
       parent::__construct($cookieFile);
       
   }
   
   private function getRequestArray(){
       if(count($this->dataRequest)){
           $retorno = '?';
           foreach ($this->dataRequest as $variavel => $valor){
               $retorno .= $variavel . '=' . $valor . '&';
           }
           return trim($retorno, '&');
       }else{
           return '';
       }
   }
   
   public function setRequestArray($array)
   {
       $this->dataRequest = $array;
   }   
   
   public function requisita($url = false){
       if(empty($url)){
           $url = $this->url;
       }
       $this->url = $url . $this->getRequestArray();
       parent::requisita($this->url);
   }
   
   protected  function options()
   {
        $headers = array("Content-Type:multipart/form-data",
            'Connection: keep-alive'
        ); // cURL headers for file uploading

      
        $this->options = array(
            CURLOPT_HEADER => true,
            CURLINFO_HEADER_OUT => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_VERBOSE => 1,
            CURLOPT_COOKIESESSION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_USERAGENT => $this->getUserAgent(),
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_UNRESTRICTED_AUTH => 1
        );
   }
}
