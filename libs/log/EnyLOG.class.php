<?php

namespace core\libs\log;

use Psr\Log\AbstractLogger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class EnyLOG extends AbstractLogger
{

    private $file = ROOT . '../z_data/ENY_LOG.log';
    private $fileError = ROOT . '../z_data/ENY_ERROR_LOG.log';

    public function log($level, $message, array $context = array()): void
    {
        $this->escreveNoArquivo($message, $level, $this->file);
    }

    public function error($message, array $context = array())
    {
        $this->escreveNoArquivo($message . ']#[Detalhes]' . $this->getDetalhes(), \Psr\Log\LogLevel::ERROR, $this->fileError);
    }

    public function getUsuario()
    {
        $id = \core\libs\login\Login::idUsuario();
        if ($id == -1) {
            return 'NO_LOGGED_USER';
        }
        return 'USER_ID:' . $id;
    }

    private function getDetalhes()
    {
        $dados = [
            'backtrace' => json_encode(debug_backtrace()),
            'POST' => json_encode($_POST),
            'SESSION' => json_encode($_SESSION),
            'userAgent' => $_SERVER['HTTP_USER_AGENT'],
            'url' => $_SERVER['REQUEST_URI']
                //'URL' =>
        ];
        return json_encode($dados);
    }

    private function escreveNoArquivo($message, $level, $arquivo)
    {
        // data atual
        $date = date('Y-m-d H:i:s');

        // formata a mensagem do log
        // 1o: data atual
        // 2o: nível da mensagem (INFO, WARNING ou ERROR)
        // 4o: usuário
        // 3o: a mensagem propriamente dita
        // 5o: uma quebra de linha
        $msg = sprintf("[%s]#[%s]#[%s]#[MESSAGE: %s]%s", $date, $level, $this->getUsuario(), $message, PHP_EOL);

        // escreve o log no arquivo
        // é necessário usar FILE_APPEND para que a mensagem seja escrita no final do arquivo, preservando o conteúdo antigo do arquivo
        file_put_contents($arquivo, $msg, FILE_APPEND);
    }

}
