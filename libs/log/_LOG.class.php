<?php
use core\libs\log\EnyLOG;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class _LOG{
    
    private static $logger = null;
    
    
    private static function iniciaLogger(){
        if(self::$logger == null){
            self::$logger = new EnyLOG();
        }
    }
    
    public static function error($message, ...$extras){
        self::iniciaLogger();
        self::$logger->error($message);
    }
    
    public static function warning(){
        self::iniciaLogger();
        self::$logger->warning($message);
    }
    
}
