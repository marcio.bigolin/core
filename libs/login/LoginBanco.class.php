<?php
namespace core\libs\login;
#TODO implementar classe.
class LoginBanco extends Login{

    private $tabelaLogin;
    /**
     * Modelo de conexão ao banco de dados
     * @var \AbstractModel 
     */
    private $modelo;
    private $campoIdUsuario;
    private $camposAceitosLogin = array('email');
    private $camposAdicionais = array();
   

    /**
     * Construtor que determina qual vai ser a chave criptográfica
     * e qual vai ser a tabela para verificar login e senha
     * 
     * @param String $chave - Chave criptográfica para salgar algoritmo
     * @param String $tabelaLogin
     */
    public function __construct($chave, \AbstractModel $model, $tabelaLogin = 'usuario') {
        $this->tabelaLogin = $tabelaLogin;
        $this->chave = $chave;
        $this->modelo = $model;
        $this->campoIdUsuario = 'id_usuario';
    }
    
    /**
     * Recebe um objeto do tipo usuario para montar a query de forma 
     * adequada.
     *  
     * @param \DTOInterface $usuario
     */
    public function setUsuarioModel(\DTOInterface $usuario){
        $this->tabelaLogin = $usuario->getTable();
    }
    
    /**
     * Retorna o valor da variável campoIdUsuario
     *
     * @return String - Valor da variável campoIdUsuario
     */
     public function getCampoIdUsuario(){
         return $this->campoIdUsuario;
     }

    /**
     * Método que seta o valor da variável campoIdUsuario
     *
     * @param String $campoIdUsuario - Valor da variável campoIdUsuario
     */
     public function setCampoIdUsuario($campoIdUsuario){
         $campoIdUsuario = trim($campoIdUsuario);
         $this->campoIdUsuario = $campoIdUsuario;
         return true;
     }
    
    /**
     * 
     * @return String nome de usuário no banco de dados
     */
    public function getUsuario(){
        return $this->userObject->getLogin();        
    }
    
    /**
     * 
     * @return int id do usuário no banco de dados
     */
    public function getIdUsuario(){
        return $this->userObject->getExtra('id');        
    }
    
    public function getEmail(){
        return $this->email;        
    }
    
    private function montaCondicao($login){
        $login = strtolower($login);
        $cond = '';
        foreach ($this->camposAceitosLogin as $campo){
            $cond .=   $campo ." ='". $login . "' OR ";
        }                
        return rtrim($cond, ' OR ');
    }
    

    public function verificaLoginSenha($login, $senha) {
        $consulta = $this->modelo->queryTable($this->tabelaLogin,
                        'senha, '. $this->campoIdUsuario . $this->getCamposAdicionais(),
                        $this->montaCondicao($login));
        $resultado = $consulta->fetchAll(\PDO::FETCH_ASSOC);
        $resultQtd =  count($resultado);
      
        if ($resultQtd == 1) {                    
            $senhaCriptografada = $resultado[0]['senha'];
            if ($this->verificaSenha($senha, $senhaCriptografada)) {
                $this->geraObjSessao($login, $senha);
                $this->userObject->addExtra('id', $resultado[0][$this->campoIdUsuario]);
                $this->userObject->setId($resultado[0][$this->campoIdUsuario]);       
                $_SESSION['user'] = serialize($this->userObject);
                return true; //Autenticação OK
            }
        } else if ($resultQtd > 1) {
            throw new \SQLException("Query montada errada ou modelagem inconssistente");
        }
        return false;
    }

    public function getNivelUsuario() {
        return $this->nivel;
    }

    /**
     * Método genérico que recupera o usuario pelo seu id
     * 
     * @param String $usuario
     * @param \AbstractModel $modelo
     */
    public function recuperaUsuario($usuario, \AbstractModel $modelo) {
        $consulta = $modelo->queryTable($this->tabelaLogin, 'usuario',
                        'id_usuario = ' . $this->idUsuario);
    } 
    
    public function setModelo(Modelo $modelo) {
        $this->modelo = $modelo;
    }
    
    /**
     * Método que prepara a tabela de l
     */
    public function gravarLogin(){
        $this->modelo->conecta(BANCO, USUARIO_BANCO, SENHA_BANCO);
           
        $this->modelo->atualizar(   $this->tabelaLogin, 
                                    'sys_ultimo_login = now()',
                                    $this->campoIdUsuario . ' = ' . $this->idUsuario );
        $this->modelo->desconecta();
    }
    
    private function getCamposAdicionais(){
        if(sizeof($this->camposAdicionais)){
            return ', ' . implode(',', $this->camposAdicionais);
        }
    }


}