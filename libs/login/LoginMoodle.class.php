<?php

namespace core\libs\login;
use core\libs\browser\BrowserGET;

/**
 * Description of LoginMoodle
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class LoginMoodle extends Login
{
    private $moodleURL = 'http://moodle.org';
    
    public function __construct($chave, $moodleUrl)
    {
        $this->moodleURL = $moodleUrl;
        parent::__construct($chave);
    }
    
    private function geraSessao($token, $user){
        $userMoodle = \MoodleUtil::requisita('core_webservice_get_site_info', null, $token);
        
        $user->addExtra('nome', $userMoodle->fullname);
        $url = str_replace('pluginfile.php','webservice/pluginfile.php',$userMoodle->userpictureurl);
        $user->addExtra('foto', $url. '&token='.$token);
        $user->addExtra('moodleToken', $token);
        $user->setId($userMoodle->userid);
      
        $_SESSION['user'] = serialize($user);
    }

    public function verificaLoginSenha($login, $senha, $revalidate = false)
    {
        if($revalidate){
            $webService = new BrowserGET();
            $webService->setUrl($this->moodleURL);
            
            $webService->setRequestArray(
                    array(
                        'username' => $login, 
                        'password' => $senha,
                        'service' => 'moodle_mobile_app'
                    ));
            $webService->requisita();
            $return = json_decode($webService->getResult());
            if(!isset($return->error)){
                $this->geraSessao( $return->token, $this->geraObjSessao($login, $senha));
                return true;
            }
            #TODO ver os erros possíveis para retornar algo mais interesante ao usuario
            return false;
        }else{
            $str = unserialize($_SESSION['user']);
            if($str->getLogin() == $login){
                return true;
            }else{
                return false;
            }
        }        
    }
}
