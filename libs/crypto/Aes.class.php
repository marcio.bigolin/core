<?php

/**
 * A implementação da classe AES é baseada na documentação oficial disponibilizada em 
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class Aes
{

    private $key;
    private $crypto;
   
    public function __construct($key = LOGIN_CHAVE)
    {
        $this->key = hash('sha256', $key);
        $this->crypto = defined('CRYPTO') ? CRYPTO : "AES-256-CBC"; 
    }
    
 
    /**
     * 
     * @param type $valor
     * @return type
     */
    public function crypt($valor)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($valor, $this->crypto, $this->key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }

    public function decrypt($cript)
    {
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($eData, $iv) = explode('::', base64_decode($cript), 2);
        $decript = openssl_decrypt($eData, $this->crypto, $this->key, 0, $iv);
        return $decript; 
    }

}
