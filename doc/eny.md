# O comando eny 

O Comando eny pode ser instalado de duas maneiras de forma global liberando o uso 

  -h                Mostra esta tela de ajuda e sai.
  -i                Instala um projeto recem clonado.
  start             Inicia o ambiente de desenvolvimento do projeto ou o padrão
  -e | eny  [porta] Inicia um servidor http com as confs do Enyalius para poder realizar testes.
  -m                Cria o básico de um projeto sem necessidade de rodar um geraProjeto.

Funções JS/Client Side
  -g | gulp         Inicia o gulp.
  -gc | gulp-core   Inicia o gulp do enyalius.
  -y | yarn         Executa o yarn com 2 args (depreciado 3.1)
  -requirejs        Executa o yarn de forma global colocando a pasta em vendor no www

Funções PHP / Server Side
  -p | pgadmin      Inicia o pgadmin conectado no projeto.
  composer          Executa composer com 2 args podendo ser install ou require

Extras
  terminator        Abre o terminator com a tela de dev pronto
  --make_tools      Cria no projeto corrente a funcionalidade de tools. 
  cript             Criptografa a pasta em ambientes debianlike
  create            Cria um projeto padrão do enyalius
"