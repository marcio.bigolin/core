/* 
 *Funções que permitem trabalhar mais facilmente com o modelo SPA (Single Page Aplication)
 */

/**
 * TODO implementar possibilidade de não ter dados no tpl ser apenas uma view
 * 
 * @param {type} tpl
 * @param {type} div
 * @param {type} dados
 * @returns {undefined}
 */
function requireTpl(tpl, div, dados) {
    var tpl = _TPL_BASE + '/' + tpl + '.html';
    $.get(tpl,
            function (data) {
                str = data;
                for (var d in dados) {
                    processa( dados[d], d);
                }
                $(div).html(str);
                $(div).removeClass('loading');
            }
    );

    function processa(obj, tipo) {
        for (var d in obj) {         
            if ((typeof obj[d]) === "object") {
                processa( obj[d], d);
            }
            var pt = tipo+"."+d;
            var rg = new RegExp("{{"+pt+"}}", 'g');
            str = str.replace(rg, obj[d]);
        }
    }
}

/**
 * Função que permite requisitar um template para inserir em uma parte de uma página através
 * de ajax.
 * 
 * @deprecated Usar a versão requireTPL (url, elemento, dados) que funciona de forma assincrona
 * @param {type} url
 * @returns {data}
 */
function requireServerTPL(url) {
    var dataReturn;
    jQuery.ajaxSetup({async: false});
    $.get(url,
            function (data) {
                dataReturn = data;
            }
    );
    jQuery.ajaxSetup({async: true});
    console.log(dataReturn);
    return dataReturn;
}

function requireJS(js){
    $('body').prepend('<script src="'+js+'"> </script>')
}
