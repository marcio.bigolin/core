/* 
 * Copyright Error: on line 4, column 29 in Templates/Licenses/license-apache20.txt
 The string doesn't match the expected date/time format. The string to parse was: "13/03/2016". The expected format was: "MMM d, yyyy". marcio.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function alturaPagina(){
    var altura = parseInt($(document).innerHeight()) ;
    return  altura  ;
}

function alturaPaginaConteudo(){
    //Se existir uma div de conteudo retorna a altura
    if($("#conteudo").length){
        return parseInt($('#conteudo').height())
    }
    var cabecalho = $("#cabecalho").length  ? parseInt($('#cabecalho').height()): 0;
    if(cabecalho == 0){
        cabecalho = $("header").length  ? parseInt($('header').height()): 0;
    }
    var rodape = $("#rodape").length ? parseInt($('#rodape').height()): 0;
       if(rodape == 0){
        rodape = $("footer").length  ? parseInt($('footer').height()): 0;
    }
    console.log(cabecalho)
    return alturaPagina() - cabecalho -rodape - 30;
}

function getLargura(){
    return parseInt($('body').css('width'))-5;
}