
/**
 * Função para editar dados 
 * 
 * @returns {undefined}
 */
function editarDado(row) {
    var id = row.replace('jqg', '');
    if (id !== null) {
        location.href = tabelaManterDados.acaoEditarDado + '/' + id;
    } else {
        alert("Selecione um dado!");
    }
}

function adicionarDado() {
    location.href = tabelaManterDados.acaoAdicionarDado;
}

function deletarDado(row) {
    var id = row.replace('jqg', '');
    if (id) {
        if (window.confirm("Tem certeza que deseja apagar este dado? \n Essa ação é irreversível.")) {
            location.href = tabelaManterDados.acaoDeletarDado + '/' + id;
        }
    } else {
        alert("Por favor selecione um dado!");
    }
}

function ajustaLargura(){
    var larg = parseInt($('#tabelaManterDados').parent().css('width'));
    var larguraAjuste = isNull(tabelaManterDados.larguraAjuste) ? 0 : tabelaManterDados.larguraAjuste
    return larg - larguraAjuste - 30;
}

function ajustaAltura(){
    
    var altura = parseInt($('#tabelaManterDados').parent().css('height'));
    if(altura < 80){
        altura = alturaPaginaConteudo();
    }
    var alturaAjuste = isNull(tabelaManterDados.altura) ? 0 : tabelaManterDados.altura;
    var alturaFinal = altura - alturaAjuste;
    return alturaFinal > 350 ? alturaFinal: 350 ;
}

$(document).ready(function()
{    
    "use strict";
    
    $.jgrid = $.jgrid || {};
    $.jgrid.no_legacy_api = true;
    $.jgrid.useJSON = true;
    $.jgrid.defaults.width = 780;

    var altura = ajustaAltura()-100;  
    console.log(altura)
    var largura = ajustaLargura();
    
    var tabelaPaginacao = "#tabelaPaginacaoManterDados";
    
    
    tabelaManterDados.colunas.unshift( { name: "act", id:"act", template: "actions", width:40, frozen:true  });
    tabelaManterDados.labelsColunas.unshift( "  ");
    calculaLarguras(tabelaManterDados);
    tabelaManterDados.colunas[0].width = 50;
    var objeto = {
        url: tabelaManterDados.dados,
        datatype: 'json',
        mtype: 'POST',
        guiStyle : 'bootstrap4',
        iconSet : 'fontAwesome',
        cmTemplate: { editable: true, autoResizable: true },
        autoResizing: { compact: true },
        colModel: tabelaManterDados.colunas,
        colNames: tabelaManterDados.labelsColunas,
        usepager: true,
        caption: tabelaManterDados.titulo,
        useRp: true,
        multiselect: !tabelaManterDados.selecaoSimples,
        pager: tabelaPaginacao,
        rowNum: 40,
        rowList: [40, 80, 120, "1000:Tudo"],
        editUrl: tabelaManterDados.dados,
        width: largura,
        height: altura,
       // toppager: true,
        viewrecords: true,
        ondblClickRow: function(rowId) {
            editarDado(rowId);
        },
        actionsNavOptions: {
            editbutton: false,
            delbutton: false,
            editRowicon: "fas fa-pen",
            editRowtitle: "Editar dado",
            deleteRowicon: "fas fa-trash",
            deleteRowtitle: "Deletar dado",
            custom: [
                { action: "editRow", position: "first", onClick: function (options) { editarDado(options.rowid) } },
                { action: "deleteRow", onClick: function (options) { deletarDado(options.rowid); } }
            ]
            }        
    };
    
    //Cria a tabela
    $("#tabelaDadosManterDados").jqGrid(objeto).jqGrid('setFrozenColumns');
    $("#tabelaDadosManterDados").jqGrid('gridResize', {
        minWidth: 500,
        maxWidth: largura,
        minHeight: 80,
        maxHeight: 1200
    });
    
        $("#tabelaManterDados").jqGrid('setGridHeight', $("#container").height() - ($('#tabelaManterDados').height() - $('#tabelaManterDados .ui-jqgrid-bdiv').height()));


    $('#tabelaDadosManterDados').navGrid( {
        search: true,
        edit: false,
        add: false,
        del: false,
        view: false,
        iconsOverText: true
    },
    {}, {}, {},
            {
                closeOnEscape: true,
                multipleSearch: true,
                closeAfterSearch: true
            } );

//
//    $('#tabelaDadosManterDados').navGrid(tabelaPaginacao, {
//        search: true,
//        edit: false,
//        add: false,
//        del: false,
//        view: false,
//        iconsOverText: true
//    },
//    {}, {}, {},
//            {
//                closeOnEscape: true,
//                multipleSearch: true,
//                closeAfterSearch: true
//            }
//    );

    //Adicionar os filtros nas colunas
    $('#tabelaDadosManterDados').jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});

//    $('#tabelaDadosManterDados').navSeparatorAdd(tabelaPaginacao,
//            {
//                sepclass: 'ui-separator',
//                sepcontent: ''
//            });

    //Botão deletar dados  
    if (tabelaManterDados.botaoDeletar) {
//        $('#tabelaDadosManterDados').jqGrid('navButtonAdd',
//                tabelaPaginacao, {
//            caption: "",
//            buttonicon: "fas fa-trash-alt",
//            onClickButton: function(row){deletarDado(row.rowid)},
//            position: "last",
//            title: "Deletar dado",
//            cursor: "hand"
//        });
    }

    //Botão editar dados                        
    if (tabelaManterDados.botaoEditar) {
//        $('#tabelaDadosManterDados').jqGrid('navButtonAdd',
//                tabelaPaginacao, {
//            caption: "",
//            buttonicon: "fas fa-pen",
//            onClickButton: editarDado,
//            position: "last",
//            title: "Editar dado",
//            cursor: "hand"
//        });

    }

    //Botão de adicionar Dados   
    if (tabelaManterDados.botaoAdicionar) {
        $('#tabelaDadosManterDados').jqGrid('navButtonAdd', {
            caption: "Adicionar novo",
            buttonicon: "fas fa-plus",
            onClickButton: adicionarDado,
            position: "last",
            title: "Adicionar dado",
            cursor: "hand"
        });
    }

    $('#tabelaDadosManterDados').navSeparatorAdd(tabelaPaginacao,
            {
                sepclass: 'ui-separator',
                sepcontent: ''
            });



});