//Sources para o concat e o uglify
module.exports = function(local) {

    this.rootJS = '';
    this.prefix = '';
    this.src = local;
    this.origens = null === arguments[1] ? new Array() : arguments[1];
    this.mainFile = 'main.js'

    this.setOrigem = function (array) {
        this.origens = new Array();
        for (var a in array) {
            this.origens.push(this.prefix + array[a])
        }
    };

    this.getDistFile = function () {
        return this.rootJS + 'dist/' + this.src + '/' ;
    };

    this.getDebugFile = function () {
        return this.rootJS + 'debug-eny/' + this.src + '/' ;
    };

    this.getCompile = function () {
        return {
            src: this.origens,
            dest: this.getDistFile()
        };
    };

    this.getDebug = function () {
        return {
            src: this.origens,
            dest: this.getDebugFile()
        };
    };

    //private functions
};
