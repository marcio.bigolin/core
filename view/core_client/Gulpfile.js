
var fs = require('fs');
PackJS = require('./compiler/PackJS.js');
ListConcat = require('./compiler/ListConcat.js');

var concatLibs = new ListConcat();

var mapa = new PackJS('componentes/mapa', [ 'js/componentes/mapa/**/*.js']);
concatLibs.add(mapa);

concatLibs.addComponente('date_picker');
concatLibs.addComponente('color_picker');
concatLibs.addComponente('frame_post');

concatLibs.add(new PackJS('enyalius', 'js/enyalius/**/*.js'));


var tabelaManterDados =   new PackJS('componentes/tabela');
   tabelaManterDados.prefix = 'js/componentes/tabela/';
   tabelaManterDados.setOrigem(['tabela.js', 'tabela_manter_dados.js']);
   tabelaManterDados.mainFile  = 'tabela_manter_dados.js';
   concatLibs.add(tabelaManterDados);
   
var tabelaRelatorio =   new PackJS('componentes/tabela');
    tabelaRelatorio.prefix = 'js/componentes/tabela/';
   tabelaRelatorio.setOrigem(['tabela.js', 'tabela_relatorio.js']);   
   tabelaRelatorio.mainFile  = 'tabela_relatorio.js';
    concatLibs.add(tabelaRelatorio);
  


// Aqui nós carregamos o gulp e os plugins através da função `require` do nodejs
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var merge = require('merge-stream');
var sass = require('gulp-sass');

//Criamos outra tarefa com o nome 'dist'
gulp.task('distjs', function () {
    console.log('--- Executando JSCompiller')
   // console.log(concatLibs.getGulpList())
    var tasks = concatLibs.getList().map(function(element){
        console.log("Executando para Lib " + element.src);
        
        return gulp.src(element.origens)
            .pipe(gulp.dest('/tmp/eny/'+element.getDebugFile()))//Copia para a pasta debug, para os mapfile funcionarem corretamente
            .pipe(sourcemaps.init())
                .pipe(gulp.dest('/tmp/'))//Seta o diretorio de trabalho
                .pipe(concat('concat'))
                .pipe(rename(element.mainFile))
                .pipe(uglify())
            .pipe(sourcemaps.write()) 
            .pipe(gulp.dest('/tmp/eny/js/'+element.getDistFile()));
    });
    return merge(tasks);
});

gulp.task('css', function () {
    console.log('--- Executando SASS')
    return gulp.src(['css/*.scss', 'css/*.css', 'css/**/*.scss', 'css/**/*.css'])
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('/tmp/eny/css'));
});

// E usamos o `gulp.watch` para o Gulp esperar mudanças nos arquivos para rodar novamente
gulp.task('watch', gulp.series(function(){
    gulp.watch(['./js/**'], gulp.parallel(['distjs']));
    gulp.watch(['./css/**'], gulp.parallel(['css']));
}));

//Criamos uma tarefa 'default' que vai rodar quando rodamos `gulp` no projeto
gulp.task('default', gulp.series(['css', 'distjs', 'watch']));