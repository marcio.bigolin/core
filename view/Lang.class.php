<?php
namespace core\view;
/**
 * Classe Estática que fornece algumas funcionalidades para trabalhar com strings internacionalizadas no sistema
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.view
 */
class Lang {

    public static $lang = '';
    private static $palavras = [];

    /**
     * Carrega as Strings salvas na pasta de intenacionalização 
     * localizado em view/templates/i18n/[lang]/strings.ini
     *
     */
    public static function load() {
        self::$palavras = parse_ini_file(TEMPLATES . '/i18n/' . self::$lang . '/STRINGS.ini', true);
    }

    /**
     * Retorna a string na lingua se disponível senão retorna a própria string
     *
     * @param String $string
     * @return String
     */
    public static function get($string) {

        if (isset(self::$palavras[$string])) {
            return self::$palavras[$string];
        }
        return $string;
    }

    /**
     * Retorna a string na lingua se disponível senão retorna a própria string
     *
     * @param String $key Chave para adicionar
     * @return String
     */
    public static function put($key, $value) {
        self::$palavras[$chave] = $valor;
    }

}
