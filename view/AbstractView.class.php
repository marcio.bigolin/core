<?php

use core\view\MensagemSistema;
use core\view\Lang;

/**
 * Description of AbstractView
 *
 * Changelog
 * 1.0.0 - Features fundamentais
 * 2.0.0 - 
 *   -Movimentação do core client para o servidor enviando apenas os arquivos compactados para a pasta do cliente
 * 2.1.0 - Movimentado o templateEngine para a pasta view.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 2.1.0
 * @package core.view
 */
abstract class AbstractView
{

    protected $templates = [];
    protected $filesJS = [];
    protected $libJS = [];
    protected $filesCSS = [];
    protected $selfScript;
    protected $varsJS = '';
    protected $selfScriptPos;

    /**
     * @var String linguagem do sistema padrão nenhuma. 
     */
    protected $lang = '';

    /**
     *
     * @var \core\view\CDNManager
     */
    protected $CDN;
    private $title;
    private $description;
    private $keywords;
    private static $render = false;

    /**
     *
     * @var \core\view\MensagemSistema
     */
    private $mensagemSistema;

    /**
     *
     * @var \core\libs\templateEngine\TemplateEngine
     */
    private $renderEngine;

    public function __construct()
    {
        $this->selfScript = '';
        $this->renderEngine = new core\view\templateEngine\TemplateEngine();
        $this->CDN = new core\view\CDNManager();
        $this->mensagemSystem();
        $this->CDN->build();
        $this->baseJs();
        $this->attValue('BASE_URL', BASE_URL);
        $this->addCSS('eny');
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Método que seta a lingua que vai ser exibida o sistema. 
     * 
     * Esse método também carrega as strings disponiveis na pasta 
     *    - app/view/i18n/lang/STRINGS.ini
     * 
     * @param type $lang
     * @return $this
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        if (count($this->templates) > 0) {
            foreach ($this->templates as $chave => $tpl) {
                $this->templates[$chave] = $this->verifyTemplate(substr($tpl, 0, strlen($tpl) - 4));
            }
        }
        Lang::$lang = $lang;        
        Lang::load();
        $this->attValueJS('LANG', $lang);
        return $this;
    }

    /**
     * Adiciona um template a tela.
     * 
     * OBS: extensão do template é inserida automaticamente
     * 
     * @param String $template caminho do template a partir da pasta de templates
     */
    public function addTemplate($template)
    {
        $this->templates[] = $this->verifyTemplate($template);
    }

    /**
     * Verifica que se existe um template na lingua setada no sistema senão retorna template padrão
     * 
     * @param type $template
     * @return type
     */
    private function verifyTemplate($template)
    {
        if (!empty($this->lang)){
           if (is_file(TEMPLATES . $template . '_' . $this->lang . '.tpl')) {
               return $template . '_' . $this->lang . '.tpl';
           }
           if(is_file(TEMPLATES .'i18n/' . $this->lang . '/' . $template . '.tpl')){
               return TEMPLATES .'i18n/' . $this->lang . '/' . $template . '.tpl';
           }
        }
        return $template . '.tpl';
    }

    /**
     *
     * @return core\view\CDNManager
     */
    public function CDN()
    {
        return $this->CDN;
    }

    public function addComponente(Componente $c)
    {
        $c->setView($this);
        $c->add();
    }

    public function addSelfScript($script, $posLibs = false)
    {
        $nl = DEBUG ? PHP_EOL : '';
        if ($posLibs) {
            $this->selfScriptPos .= $script . $nl;
        } else {
            $this->selfScript .= $script . $nl;
        }
    }

    /**
     * Usar addCSS se for lib a mesma deve ser carregada pelo gerenciador do 
     * CDN do enyalius
     * 
     * @param type $css
     * @deprecated since version 2.0
     */
    public function addLibCSS($css)
    {
        $this->addCSS($css);
    }

    public function addLibJS($js, $path = null)
    {
        if (is_null($path)) {
            $path = '/js/dist/';
        }
        $this->libJS[$js] = $path . $js;
    }

    /**
     * Método que permita adicionar um css por caminho absoluto. 
     * 
     * Útil para adicionar CDNs não padronizados, ou css gerado pelo sistema.
     * 
     * @param type $src
     * @param [String $media] Midia type accepted por exemplo print
     */
    public function addAbsoluteCSS($src, $media = ''){
        $add = !empty($media)? 'media="'. $media .'"' : $media;
        $this->filesCSS[$src] = ['src' => $src, 'extra'=> $add];
    }
    
    /**
     * Método que adiciona um recurso de css a view atual. 
     * 
     * @param type $css
     * @param [String $media] Midia type accepted por exemplo print
     */
    public function addCSS($css, $media = '')
    {
        $src = '/css/' . $css . '.css'; 
        $this->addAbsoluteCSS($src, $media);
    }

    public function addJS($js)
    {
        $path = DEBUG ? '' : 'dist/';
        $this->filesJS[$js] = $path . $js;
    }

    /**
     * Seta os valores das variaveis do Template a ser carregado pelo visualizador.
     *
     * @param String  $variavel = Nome da variavel que será setada
     * @param "object"  $valor = Valor para setar variável.     *
     */
    public function attValue($variavel, $valor)
    {
        return $this->renderEngine->assign($variavel, $valor);
    }

    /**
     * Cria uma variável em javaScript com o valor que recebe como argumento se 
     * for um array ou um objeto transforma esse em um objeto JavaScript e atribui
     * para a váriavel definida como argumento 1.
     *
     * @param String  $variavel  Nome da variável que será setada
     * @param misc  $valor  Valor para setar variável.     
     */
    public function attValueJS($variavel, $valor)
    {
        if (is_array($valor) || is_object($valor)) {
            $valor = json_encode($valor);
            $error = json_last_error();
            if ($error) {
                throw new JSONErrorException($error);
            }
        } else if (!is_numeric($valor)) {
            $valor = '"' . $valor . '"';
        }
        $this->varsJS .= 'var ' . $variavel . ' = ' . $valor . ';' . PHP_EOL;
    }

    /**
     * Método que seleciona todos os templates scripts e css e gera a tela.
     * 
     */
    public function render()
    {
        self::$render = true;
        $this->attDefault();
        $this->show(CORE . 'view/templates/generic/header.tpl');
        $this->mostrarTemplatesNaTela();
        $this->show(CORE . 'view/templates/generic/footer.tpl');
    }

    /**
     * Método que renderiza a página em formato ajax e não em formato html padrão 
     * adicionando todos os templates default scripts e css.
     */
    public function renderAjax()
    {
        self::$render = true;
        $this->mostrarTemplatesNaTela();
    }

    /**
     * Método que remove todos os templates colocados por padrão em um visualizador
     * pode ser útil para evitar renderizar coisas desnecessárias.
     * 
     */
    public function resetTemplates()
    {
        $this->templates = array();
    }

    public function isRender()
    {
        return self::$render;
    }

    public function setRenderizado($renderizado = true)
    {
        self::$render = $renderizado;
    }

    /**
     * Método que mostra na tela o template adicionado em endereço
     *
     * @param String  $endereco = Endereco fisico do arquivo a ser aberto.
     */
    public function show($endereco)
    {
        try {
            $this->renderEngine->display($endereco);
        } catch (SmartyException $e) {
            $erro = new ErrorHandler();
            $erro->logarExcecao($e);
        }
        return;
    }

    /**
     * Retorna uma string com o resultado dos templates processados.
     * 
     * @return String página com as variáveis processadas
     */
    public function resultAsString()
    {
        $strResult = '';
        if (sizeof($this->templates) > 0) {
            foreach ($this->templates as $template) {
                $strResult .= $this->renderEngine->fetch($template);
            }
        }
        return $strResult;
    }

    /**
     * Adiciona um template com cabeçalho de formulário
     * 
     * @param string $action
     * @param string $id - default = formPrincipal
     */
    public function startForm($action, $id = 'formPrincipal')
    {
        $this->addTemplate(CORE . 'view/templates/generic/start_form');
        $this->addLibCSS('componentes/forms');
        $this->attValue('action', $action);
    }

    /**
     * Método que termina um formulário colocando os botões padrões de acordo com 
     * a lingua definida.
     * 
     * @param bool $botoes - se false não mostra os botões padrões
     */
    public function endForm($botoes = true)
    {
        if(!$botoes){
            $this->attValue('WITHOUT_BUTTONS', true);
        }
        $this->addTemplate(CORE . 'view/templates/generic/end_form');
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function addMensagemErro($mensagem)
    {
        $this->mensagemSistema->setTipoMensagem('ERRO');
        $this->mensagemSistema->setMensagem(Lang::get($mensagem));
    }

    /**
     * 
     * @param unknown $erro
     */
    public function addErro($erro)
    {
        $this->mensagemSistema->addErro($erro);
    }

    public function addMensagemSucesso($mensagem)
    {
        $this->mensagemSistema->setTipoMensagem('SUCESSO');
        $this->mensagemSistema->setMensagem($mensagem);
    }

    /**
     * Adiciona um array com erros para ser mostrado na tela. 
     * Caso seja passado apenas uma string adiciona o erro na pilha de erros.
     * 
     * @param Misc $erro
     */
    public function addErros($erro)
    {
        $this->mensagemSistema->setTipoMensagem('ERRO');
        if (is_array($erro)) {
            $this->mensagemSistema->setMensagem('Os seguintes erros necessitam da sua atenção.');
            $this->mensagemSistema->addListaErro($erro);
        } else {
            $this->mensagemSistema->addErro($erro);
        }
    }

    /**
     * Mostra todos os templates que estão no Buffer de templates.
     */
    private function mostrarTemplatesNaTela()
    {
        if (sizeof($this->templates) > 0) {
            foreach ($this->templates as $template) {
                $this->show($template);
            }
        }
    }

    private function attDefault()
    {
        $this->attValue('TITLE', $this->title);
        $this->attValue('DESCRIPTION', $this->description);
        $this->attValue('KEYWORDS', $this->keywords);
        $this->attCDN();
        $this->attCSS();
        $this->attJS();
    }

    private function attCSS()
    {
        $this->attValue('filesCSS', $this->filesCSS);
    }

    private function attJS()
    {
        $this->attValue('libsJS', $this->libJS);
        $this->attValue('scripts', $this->filesJS);
        $this->attValue('selfScript', $this->varsJS . PHP_EOL . $this->selfScript);
        $this->attValue('selfScriptPos', $this->selfScriptPos);
    }

    private function attCDN()
    {
        $this->attValue('CDN', $this->CDN);
    }

    public function mensagemSystem(MensagemSistema $m = null)
    {
        $this->mensagemSistema = $m == null ? new MensagemSistema() : $m;
        $this->attValue('MSG', $this->mensagemSistema);
        $this->attValue('MSG_FILE', $this->mensagemSistema->getTemplate() . '.tpl'); //Variável para dar include do template
    }

    public function getMensagemSystem()
    {
        return $this->mensagemSistema;
    }

    private function baseJs()
    {
        $this->CDN()->add('jquery');
        $this->addLibJS('enyalius/main');
    }

}
