<?php

namespace core\view;

/**
 * Classe que gerencia as bibliotecas com suporte a CDN e as carrega diretamente 
 * para auxiliar no desempenho.
 * 
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class CDNManager
{

    private $css = array();
    private $js = array();
    private $json = false;
    private $libVerify = array();
    private $offline = false;

    public function __construct()
    {
        if (defined('OFFLINE')) {
            $this->offline = OFFLINE;
        }
    }

    public function build($file = null)
    {
        if ($file == null) {
            $file = $this->defineCaminho();
        }
        $this->json = json_decode(file_get_contents($file), true);
    }

    public function getCSS()
    {
        return $this->css;
    }

    public function getJs()
    {
        return $this->js;
    }
    
    public function addExtra($extra, $type = 'js'){
        if($type == 'js'){
            $this->js[] = new cdn\CDNNode($extra);
        }else{
            $this->css[] = new cdn\CDNNode($extra);
        }
    }

    public function add($lib)
    {
        if ($this->isAdd($lib)) {
            return;
            #TODO verificar o quanto prejudica a performance e se é necessário logar iiso
        }
        $this->libVerify[strtolower($lib)] = 1;
        $cdnFind = 0;
        if (isset($this->json['js'][$lib])) {
            $this->js[] = $this->getCDN($lib);
            $cdnFind = 1;
        }
        if (isset($this->json['css'][$lib])) {
            $this->css[] = $this->getCDN($lib, 'css');
            $cdnFind = 1;
        }
        return $cdnFind;
    }

    public function isAdd($lib)
    {
        if (isset($this->libVerify[strtolower($lib)])) {
            return true;
        }
    }

    private function defineCaminho()
    {
        return CORE . 'view/core_client/cdn.json';
    }

    /**
     *
     * @param type $libName
     * @return String URI CDN de uma biblioteca
     */
    private function getCDN($libName, $type = 'js')
    {
        if (!$this->json) {
            $this->build();
        }
        if (DEBUG){
            if( isset($this->json[$type][$libName]['debug'])) {
                return $this->cdnDebug($libName, $type);
            }else if(isset($this->json[$type][$libName]['alternative'])){
                return $this->getOfflineScript($this->json[$type][$libName]);
            }
        }
        if (isset($this->json[$type][$libName]) && !$this->offline) {
            return new cdn\CDNNode($this->json[$type][$libName]);
        } else {
            return new cdn\CDNNode();
        }
    }
    
    private function getOfflineScript($lib){
        return new cdn\CDNNode( '/vendor/' . $lib['alternative']);
    }

    private function cdnDebug($libName, $type = 'js')
    {
        $debug = $type == 'js' ? '/debug' : '/';
        return new cdn\CDNNode( '/'. $type . $debug . '/' . $libName . '/' . $libName . '.' . $type);
    }
    
    private function getServerName(){
        return '/';
        $pt = $_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http';
        return  $pt . '://' . $_SERVER['SERVER_NAME'] . '/';
    }

}
