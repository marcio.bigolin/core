<?php


/**
 * A classe HTMLEditor permite que seja adicionado o componente Froala em textAreas
 * tornando em um poderoso editor WYSIWYG HTML
 * 
 * Mais informações em https://www.froala.com/wysiwyg-editor
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @package core.componentes
 * @version 1.0.0 10/12/2019
 */
class HTMLEditor extends Componente{
    protected static $adicionado = false;
    private static $cont = 0;
    private $id;

    public function __construct($id){
        $this->id = $id;
    }
    
    public function add() {
        if(!self::$adicionado){
            $this->view->addLibJS('summernote-bs4.min', '/vendor/summernote/dist/');
            $this->view->addAbsoluteCSS('/vendor/summernote/dist/summernote-bs4.min.css');

            self::$adicionado = true;
            $this->view->addSelfScript('var editor = new Array();', true);
        }        
        $this->view->addSelfScript('editor['.self::$cont.'] =   $(("'.$this->id. '")).summernote( {toolbar: [
          ["style", []],
          ["font", ["bold", "underline", "italic", "clear"]],
          ["color", ["color"]],
          ["para", ["ul", "ol", "paragraph"]],
          ["table", []],
          ["insert", ["link"]],
          ["view", ["help"]]
        ]});', true);
        self::$cont++;
    }
}
