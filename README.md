Sugira algo pode mandar um e-mail <a href="mailto:incoming+enyalius/core+czxzBjGgMzsCa8KnpyfY@gitlab.com">incoming+enyalius/core+czxzBjGgMzsCa8KnpyfY@gitlab.com </a> 

# Como começar
* [Como instalar](doc/install.md)
* [CRUD Usuário](doc/getStarted.md)
* [Usando o comando eny](doc/eny.md)

# CORE
O CORE é o núcleo de desenvolvimento em MVC do PHP. Esse núcleo é composto de pequenas
classes que tem por função melhorar o desenvolvimento de aplicações web. As libs são instaladas a partir 
do composer. 

O Core_client agora é parte integrante do CORE visto que os códigos são compilados pelo uglify usando o gulp e colocados nas pastas do cliente. 

Todos os comandos e utilização do enyalius pode ser encontrado no script `./enyalius` se quiser ver como usar ele em seu computador experimente.

# Tools
O pacote [tools](http://gitlab.com/enyalius/tool) extende as funcionalidades do 
Framework e possui gerador de crud entre outros. A documentação completa dessa funcionalidade
fica disponível em [tool](doc/Tools.md)

# Funcionalidades e dependências
<img src="doc/conceitual.jpg" />

