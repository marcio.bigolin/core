<?php

/**
 * Description of ValidatorUtil
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 * @version 1.0
 * @package 
 */
abstract class ValidatorUtil
{
        /**
     *
     * @param Misc $var
     * @return String 
     */
    public static function variavel($var) {
        $var = trim($var);
        $var = filter_var($var, FILTER_SANITIZE_STRING);
        return preg_replace('[^aA-zZ/0-9çãõéíóáúêô_]', '', $var);
    }

    /**
     * Método que utiliza o filter sanitize para tratar uma variavel do tipo int
     * 
     * @param Misc $var
     * @return int 
     */
    public static function variavelInt($var) {
        $var = filter_var($var, FILTER_SANITIZE_NUMBER_INT);
        return (int) $var;
    }
    
    /**
     * Recebe um array e trata os dados o método é recursivo caso receba um 
     * array.
     * 
     * @param Array $array
     * @param Array $ignore
     * @return Array
     */
    public static function trataArrayDados($array, $ignore) {
        foreach ($array as $key => $value) {
            if(in_array($key, $ignore)){
                continue;
            }
            if (is_array($value)){ 
                $array[$key] = $this->trataArrayDados($value);                
            }else if (is_int($value)){ 
                $array[$key] = filter_var($value, FILTER_SANITIZE_NUMBER_INT); 
            } else if (is_float($value)){ 
                $array[$key] = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
            } else if (is_string($value)){ 
                $array[$key] = filter_var($value, FILTER_SANITIZE_STRING);                 
            } else {
                $array[$key] = filter_var($value, FILTER_DEFAULT); continue;
            }
        }        
        return $array;
    }
    
    public static function sanitizeForm(...$ignore){
        $dados = self::trataArrayDados($_POST, $ignore);
        return $dados;
    }
}
