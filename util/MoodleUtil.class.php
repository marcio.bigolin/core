<?php

/**
 * Classe estatica que facilita a manipulação do webService do moodle
 *
 * @author Marcio Bigolin <marcio.bigolinn@gmail.com>
 */
class MoodleUtil
{

    /**
     * Token que permite sucessivas consultas ao web service do moodle
     * 
     * @var String
     */
    public static $token = false;

    /**
     *
     * @var String 
     */
    public static $site = 'https://moodle.canoas.ifrs.edu.br';

    public static function geraToken($usuario, $senha)
    {
        $url = 'https://' .self::$site . '/login/token.php';
        $url .= '?username=' . $usuario . '&password=' . $senha .  '&service=moodle_mobile_app';    
    }

    /**
     * 
     * 
     * @param String $servico
     * @param Array(optional) $extras
     * @param String(optional) $token 
     * @return stdObject Objeto de retorno do serviço
     */
    public static function requisita($servico, $extras = '', $token = false)
    {       
        $extrasOK = self::preparaUrlExtras($extras);
      
        $tokenOK = $token ? $token : self::$token;
        $url = self::$site . '/webservice/rest/server.php?wsfunction=' . $servico . $extrasOK . '&wstoken=' . $tokenOK . '&moodlewsrestformat=json';
        return json_decode(file_get_contents($url));
    }

    private static function preparaUrlExtras($extras)
    {
        if(is_array($extras)){
           $retorno = '';
           foreach ($extras as $variavel => $valor){
               $retorno .= $variavel . '=' . $valor . '&';
           }
           return trim($retorno, '&');
        }        
        return $extras;
    }

}
